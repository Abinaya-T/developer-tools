const axios = require('axios');
const objectsToCsv = require('objects-to-csv');
const fs = require('fs');

function getApiURL() {

	let url = new URL("https://leetcode.com/api/problems/all/");
	return url.toString();
}

async function getAllProblems() {
	try {
	let response = await axios.get(getApiURL());
	return response.data;
	}
	catch(e){console.error(e)};
}

function getTopHundredProblems(allProblems) {

	const allProb = allProblems.stat_status_pairs
					.filter(usr => usr.paid_only == false)
					.map(val =>({ id: val.stat.frontend_question_id,  question_title: val.stat.question__title, submissions: val.stat.total_submitted}))
					.sort((a,b) => b.submissions - a.submissions );

	const topProb = allProb.slice(0,100);
	return topProb;

}


async function createCSV(topHundredProblems) {
    // Write data to a CSV file
	// Input:
	//  	topHundredProblems - data to write
	const csv = new objectsToCsv(topHundredProblems);
	await csv.toDisk('./list.csv');

}

async function main() {
    console.log("Running main");
    const allProblems = await getAllProblems();
    if (allProblems != null) {
		fs.writeFile("./problemsAll.json", JSON.stringify(allProblems, null, 4), (err) => {
			if (err) {
				console.error(err);
				return;
			}
	   });
	}

    const topHundredProblems = await getTopHundredProblems(allProblems);
    createCSV(topHundredProblems);
}

module.exports = {getApiURL, getAllProblems, getTopHundredProblems, createCSV, main};
